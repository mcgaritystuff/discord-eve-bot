module.exports = (() => {
  if (process.argv.length > 2 && process.argv[2] === 'prod') {
    return require('./prod/configuration');
  } else {
    return require('./dev/configuration');
  }
})();