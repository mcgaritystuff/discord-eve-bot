module.exports = (() => {
  if (process.argv.length > 2 && process.argv[2] === 'prod') {
    return require('./prod/keys');
  } else {
    return require('./dev/keys');
  }
})();