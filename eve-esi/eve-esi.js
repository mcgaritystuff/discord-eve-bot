// Handle all our own Eve API stuff here!
const Axios = require('axios');
const Keys = require('../config/keys');
const esiHostUri = "esi.evetech.net";
const esiBaseUri = `https://${esiHostUri}`;
const esiUri = `${esiBaseUri}/latest`;
const eveHostLoginUri = "login.eveonline.com";
const eveBaseLoginUri = `https://${eveHostLoginUri}`

const allianceCorporations = async (allianceId) => callEsi(`${esiUri}/alliances/${allianceId}/corporations`);
const allianceIcons = async (allianceId) => callEsi(`${esiUri}/alliances/${allianceId}/icons`);
const allianceInfo = async (allianceId) => callEsi(`${esiUri}/alliances/${allianceId}`);
const categories = async (categoryId) => callEsi(`${esiUri}/universe/categories/${categoryId}`);
const characterInfo = async (characterId) => callEsi(`${esiUri}/characters/${characterId}`);
const characterPortrait = async (characterId) => callEsi(`${esiUri}/characters/${characterId}/portrait`);
const constellationInfo = async (constellationId) => callEsi(`${esiUri}/universe/constellations/${constellationId}`);
const corporationIcons = async (corporationId) => callEsi(`${esiUri}/corporations/${corporationId}/icons`);
const corporationInfo = async (corporationId) => callEsi(`${esiUri}/corporations/${corporationId}`);
const groups = async (groupId) => callEsi(`${esiUri}/universe/groups/${groupId}`);
const races = async () => callEsi(`${esiUri}/universe/races`);
const skillQueue = async (characterId, authToken) => callEsi(`${esiUri}/characters/${characterId}/skillqueue?token=${authToken}`);
const search = async (searchString, category, strictSearch) => callEsi(`${esiUri}/search?categories=${category}&search=${searchString}&strict=${strictSearch}`);
const solarSystemInfo = async (solarSystemId) => callEsi(`${esiUri}/universe/systems/${solarSystemId}`);
const types = async (typeId) => callEsi(`${esiUri}/universe/types/${typeId}`);

const getAlliance = async (allianceId) => {
  return new Promise(async function(resolve, reject) {
    const info = await allianceInfo(allianceId)
    const icons = await allianceIcons(allianceId);
    const corporations = await allianceCorporations(allianceId);
    if (isError(info) || isError(icons) || isError(corporations)) {
      reject({error: {info, icons, corporations}});
    }
    resolve({
      info,
      icons,
      corporations
    });
  });
};

const getCharacter = async (characterId) => {
  return new Promise(async function(resolve, reject) {
    const info = await characterInfo(characterId)
    const portrait = await characterPortrait(characterId);
    const corporation = await corporationInfo(info.corporation_id);
    
    if (isError(info) || isError(portrait) || isError(corporation)) {
      reject({error: {info, portrait, corporation}});
    }
    resolve({
      info,
      portrait,
      corporation
    });
  });
};

const getCorporation = async (corporationId) => {
  return new Promise(async function(resolve, reject) {
    const info = await corporationInfo(corporationId)
    const icons = await corporationIcons(corporationId);
    
    if (isError(info) || isError(icons)) {
      reject({error: {info, icons}});
    }
    resolve({
      info,
      icons
    });
  });
};

const playerAuthToken = async (playerRefreshToken) => {
  return Axios.post(
    `${eveBaseLoginUri}/oauth/token`, 
    {
      grant_type: 'refresh_token',
      refresh_token: playerRefreshToken
    },
    {
      headers: {
        'Authorization': `Basic ${Keys.Base64Auth}`,
        'Content-Type': 'application/json',
        'Host': eveHostLoginUri
      }
    }
  ).then(response => {
    return response.data.access_token;
  }).catch( error => {
    return { errorMessage: error };
  });
};

const verifyProfile = async (refreshToken) => {
  let authToken;

  try {
    authToken = await playerAuthToken(refreshToken);
  } catch (error) {
    return { errorMessage: authToken };
  }

  return Axios.get(`${esiBaseUri}/verify`, {
    headers: {
      'Authorization': `Bearer ${authToken}`,
      'Host': esiHostUri
    }
  }).then( response => {
    return response.data;
  }).catch( error => {
    return { errorMessage: error };
  });
};

const callEsi = async (url) => {
  return Axios.get(url).then(response => {
    return response.data;
  }).catch(error => {
    return { errorMessage: error };
  });
};

const isError = (response) => {
  return response.errorMessage != null;
};

module.exports = {
  allianceCorporations,
  allianceIcons,
  allianceInfo,
  categories,
  characterInfo,
  characterPortrait,
  constellationInfo,
  corporationIcons,
  corporationInfo,
  getAlliance,
  getCharacter,
  getCorporation,
  groups,
  playerAuthToken,
  races,
  search,
  skillQueue,
  solarSystemInfo,
  types,
  verifyProfile
};