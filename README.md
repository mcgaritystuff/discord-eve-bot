# Setup Application

First, you will need the application setup on Eve Online. Please go to the following URL, login
with your EVE credentials, and create an application. This is how you will get your client ID
and manage the permissions to your app: https://developers.eveonline.com

To install:

1. Create a folder in the /config directory (named 'dev' or 'prod', depending on the environment)
1. In the folder, create the following two files:

# keys.js

```js
module.exports = {
  "ClientId": "EVE-CLIENT-ID",
  "Token": "YOUR-DISCORD-BOT-TOKEN",
  "Database": "MONGO-DATABASE-URL-WITH-USER:PASS",
  "Base64Auth": "Base64(clientId:EveAppSecretKey)",
  "SimpleEncryptHash": "WHATEVER-CRAZY-HASH-YOU-WANT-HERE",
  "EveSecretKey": "EVE-SECRET-KEY"
};
```

# configuration.js

```js
module.exports = {
  // Please see this url for SSO setup:
  // http://eveonline-third-party-documentation.readthedocs.io/en/latest/sso/authentication.html
  "DiscordSymbol": "$", // The symbol the bot listens to for the first character in a message. Defaults to $ if not assigned
  "CallbackUrl": "https://www.mysuperduperawesomewebsite.com/callback",
  "RegisterUrl": "https://www.mysuperduperawesomewebsite.com/register",
  "RolesAllowedToReadUsers": ['roleId', 'role2Id'], // Only these user role ids can view skills of other players
  "WelcomeChannelId": "DiscordChannelId"
};
```

Finally, run the following before you start:
`npm run initialize-prod` (or `initialize-dev`)

After the full initialization completes, you should be able to do `npm run prod` (or `dev`)