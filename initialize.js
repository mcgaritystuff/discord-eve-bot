const Mongoose = require('mongoose');
require('./models/Race');
require('./models/Skill');
 
const Keys = require('./config/keys');

const Race = Mongoose.model('races');
const Skill = Mongoose.model('skills');

const NUMBER_OF_RACES = 4;
const SKILL_CATEGORY_ID = 16;

const eveEsi = require('./eve-esi/eve-esi');

Mongoose.connect(Keys.Database, { useNewUrlParser: true, useUnifiedTopology: true });
Mongoose.connection.once('open', Initialize);

/**
 * This initializes any database items we need to initialize for the game to
 * ease off the API and such.
 */
async function Initialize() {
  console.log('Initializing database...');
  await createRaces();
  await createSkills();
  console.log('Complete!');
  process.exit(0);
}

/**
 * Creates all the races for EVE into our database to avoid an API call
 */
async function createRaces() {
  console.log('Initializing races...');
  let existingRaces = await Race.find({});

  if (existingRaces.length === NUMBER_OF_RACES) {
    console.log('Races already initialized!');
    return;
  }
  
  let races = await eveEsi.races();

  await races.forEach(async race => {
    let existingRace = await Race.findOne({raceId: race.race_id});
    if (!existingRace) {
      await Race.create({
        raceId: race.race_id,
        allianceId: race.alliance_id,
        name: race.name,
        description: race.description
      })
    }
  });

  console.log('Done initializing races');
}

/**
 * Creates all the skills for EVE into our database to avoid major API calls
 */
async function createSkills() {
  console.log('Initializing skills...');
  let existingSkills = await Skill.find({});
  let allSkillIds = [], allSkills = [];
  let skillCategory = await eveEsi.categories(SKILL_CATEGORY_ID);
  if (!skillCategory) {
    console.log('ERROR INITIALIZING SKILLS');
    return;
  }

  console.log('Grabbing all skill ids from API...');
  for(let i = 0; i < skillCategory.groups.length; i++) {
    let skills = await eveEsi.groups(skillCategory.groups[i]);
    allSkillIds = allSkillIds.concat(skills.types);
  }
  
  if (allSkillIds.length !== existingSkills.length) {
    console.log(`Skill count length doesn't match! ${allSkillIds.length} vs. ${existingSkills.length}`);
    console.log('Clearing old database entries...');
    await Skill.deleteMany({});

    console.log('Now adding all skill entries (0% complete)...');
    for (let i = 0; i < allSkillIds.length; i++) {
      if (i % 10 === 0 && i > 0) {
        console.log(`${i}/${allSkillIds.length} (${(i/allSkillIds.length*100).toFixed(2)}% complete)...`);
      }
      let skill = await eveEsi.types(allSkillIds[i]);
      allSkills = allSkills.concat({
        skillId: skill.type_id,
        skillName: skill.name,
        skillDescription: skill.description
      });
    }
    console.log('Now adding to the database (100% complete)...');
    await Skill.insertMany(allSkills);
  } else {
    console.log('Number of entries for skills still matches!');
  }
}