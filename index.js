const Mongoose = require('mongoose');
require('./models/Alliance');
require('./models/Corporation');
require('./models/Player');
require('./models/Race');
require('./models/Skill');
 
const express = require('express')
const EveBot = require ('./bot/eve_bot').EveBot;
const Keys = require('./config/keys');
const RegisterListener = require('./register/register').RegisterListener
const CallbackListener = require('./register/register').CallbackListener

Mongoose.connect(Keys.Database, { useNewUrlParser: true, useUnifiedTopology: true });
Mongoose.connection.once('open', startServer);

async function startServer() {
  let databaseInitialized = await ValidationInitialization();
  if (!databaseInitialized) {
    process.exit(-1);
  }
  
  const eveBot = new EveBot(Keys.Token);
  const Client = eveBot.login();

  Client.on('ready', msg => {
    eveBot.ready();
  });

  Client.on('message', msg => {
    eveBot.handleMessages(msg);
  }); 

  Client.on('guildMemberAdd', member => {
    eveBot.welcomeUser(member);
  });

  Client.on('error', console.error);

  const app = express();
  app.get('/register/:id', RegisterListener);
  app.get('/callback', CallbackListener)
  app.listen(3000, () => console.log('Server running on port 3000'));
}

/**
 * Validates that the database has been initialized.
 * @returns {Boolean} - True = initialized, False = not properly initialized
 */
async function ValidationInitialization() {
  const Race = Mongoose.model('races');
  const Skill = Mongoose.model('skills');
  
  let races = await Race.find({});
  let skills = await Skill.find({});
  
  if (races.length === 0 || skills.length === 0) {
    console.log('Please initialize database with `npm run initialize-dev` or `npm run initialize-prod`.');
    return false;
  }

  console.log('Database properly initialized');
  return true;
}