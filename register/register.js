const Axios = require('axios')
const Configuration = require('../config/configuration');
const Keys = require('../config/keys');
const Mongoose = require('mongoose');
const Player = Mongoose.model('players');

const scope ="publicData%20esi-calendar.read_calendar_events.v1%20esi-location.read_location.v1%20esi-location.read_ship_type.v1%20esi-mail.read_mail.v1%20esi-skills.read_skills.v1%20esi-skills.read_skillqueue.v1%20esi-wallet.read_character_wallet.v1%20esi-clones.read_clones.v1%20esi-characters.read_contacts.v1%20esi-bookmarks.read_character_bookmarks.v1%20esi-assets.read_assets.v1%20esi-planets.manage_planets.v1%20esi-fleets.read_fleet.v1%20esi-fittings.read_fittings.v1%20esi-characters.read_loyalty.v1%20esi-characters.read_opportunities.v1%20esi-characters.read_chat_channels.v1%20esi-characters.read_medals.v1%20esi-characters.read_standings.v1%20esi-characters.read_agents_research.v1%20esi-industry.read_character_jobs.v1%20esi-markets.read_character_orders.v1%20esi-characters.read_blueprints.v1%20esi-location.read_online.v1%20esi-contracts.read_character_contracts.v1%20esi-clones.read_implants.v1%20esi-characters.read_fatigue.v1%20esi-characters.read_notifications.v1%20esi-industry.read_character_mining.v1%20esi-corporations.read_medals.v1%20esi-characters.read_fw_stats.v1%20esi-characterstats.read.v1%20esi-search.search_structures.v1%20esi-universe.read_structures.v1";
const eveUrl = "https://login.eveonline.com/oauth/authorize/?response_type=code";

exports.RegisterListener = async function(req, res) {
  const redirectUri = encodeURI(Configuration.CallbackUrl);
  const clientId = Keys.ClientId;
  const idNum = req.params['id']
  const discordId = doSimpleDecrypt(idNum, Keys.SimpleEncryptHash)
  
  const state = randomString();
  let player = await Player.findOne({ playerDiscordId: discordId });

  if (player && !player.playerSSOCode) {
    await Player.updateOne(player, {
      playerSSOState: state
    });
  } else if (!player) {
    await Player.create({
      playerDiscordId: discordId,
      playerSSOCode: '',
      playerSSOState: state
    });
  } else {
    res.send("You already exist in the database. Please contact an admin.");
    return;
  }
  
  const fullRedirectUri = `${eveUrl}&redirect_uri=${redirectUri}&client_id=${clientId}&scope=${scope}&state=${state}`
  res.writeHead(302, {
    'Location': fullRedirectUri
  });
  res.end();
};


exports.CallbackListener = async function (req, res) {
  const state = req.query['state'];

  let player = await Player.findOne({ playerSSOState: state });
  if (!player) {
    res.send("You are unauthorized. What are you doing, man??");
    return;
  }

  const oauthUrl = "https://login.eveonline.com/oauth/token";
  const codeFromUri = req.query['code'];
  let initialRefreshToken;
  let confirmedRefreshToken;
  
  // Get the First Refresh Token with the Auth Code
  const firstRequest = await Axios.post(
    oauthUrl,
    {
      grant_type: 'authorization_code',
      code: codeFromUri
    },
    {
      headers: {
        'Authorization': `Basic ${Keys.Base64Auth}`,
        'Content-Type': 'application/json',
        'Host': 'login.eveonline.com'
      }
    }
  ).then(response => {
    initialRefreshToken = response.data.refresh_token;
  }).catch(error => {
    res.send("There was an error getting authenticated. Please contact an admin.");
    return error.response;
  });

  if (firstRequest && firstRequest.status >= 400) {
    console.log(firstRequest.data);
    return;
  }

  // Now get the Refresh Token to save
  const secondRequest = await Axios.post(
    oauthUrl,
    {
      grant_type: 'refresh_token',
      refresh_token: initialRefreshToken
    },
    {
      headers: {
        'Authorization': `Basic ${Keys.Base64Auth}`,
        'Content-Type': 'application/json',
        'Host': 'login.eveonline.com'
      }
    }
  ).then(response => {
    confirmedRefreshToken = response.data.refresh_token;
  }).catch(error => {
    res.send("There was an error trying to confirm your request. Please contact an admin.");
    return error.response;
  });

  if (secondRequest && secondRequest.status >= 400) {
    console.log(secondRequest.data);
    return;
  }

  await Player.updateOne(player, {
    playerSSOCode: confirmedRefreshToken
  });
  
  res.send("You're good to go! You can close this window.")
};
    
function doSimpleDecrypt(encryptedId, hash) {
    let decryptedId = '';
    for (let i = 0, j = 0; i < encryptedId.length; i += 2, j++) {
        const hex = encryptedId[i] + encryptedId[i+1];
        const resultNumber = parseInt(hex, 16);
        const correctNumber = resultNumber - hash[j].charCodeAt(0);
        decryptedId += `${correctNumber}`;
    }
    
    return decryptedId;
}

function randomString() {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}