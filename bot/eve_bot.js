const Discord = require('discord.js');

const Configuration = require('../config/configuration');

// All the bot commands
const {PrintAllianceInfo} = require('./commands/alliance');
const {PrintCorporationInfo} = require('./commands/corporation');
const {PrintHelp} = require('./commands/print_help');
const {PrintProfile} = require('./commands/print_profile');
const {PrintSkillQueue} = require('./commands/queue');
const {PrintSolarSystemInfo} = require('./commands/solar_system');
const {RegisterUser} = require('./commands/register');

// Create the Discord Client
const Client = new Discord.Client();

class EveBot {  
  /**
   * Creates an EveBot for the server
   * @param {String} clientSecret - The Discord Secret Token for the bot
   */
  constructor(clientSecret) {
    this.clientSecret = clientSecret;
  }

  /**
   * Logs the EveBot into Discord
   * @returns {Object} - The Discord Client
   */
  login() {
    Client.login(this.clientSecret);

    return Client;
  }
  
  /**
   * Posts to the console that it's logged in
   */
  ready() {
    console.log(`logged in as ${Client.user.username}!`);
  }

  /**
   * Checks for commands given in the Discord channel using the respective message
   * @param {Object} message - The Discord Message to handle and check for commands
   */
  handleMessages(message) {
    const firstchar = Configuration.DiscordSymbol || '$';
    // Only read messages that start with the right symbol for the Eve Bot and ignore self
    if (message.content[0] !== firstchar || message.author.id === Client.user.id) {
      return;
    }

    const messageContents = message.content.split(" ");

    const firstMessage = messageContents[0];
    const secondMessage = messageContents[1];
    
    switch (firstMessage.toLowerCase()) {
      case `${firstchar}alliance`:
      case `${firstchar}a`:
        PrintAllianceInfo(message);
        break;
      case `${firstchar}corporation`:
      case `${firstchar}c`:
        PrintCorporationInfo(message);
        break;
      case `${firstchar}help`:
      case `${firstchar}h`:
        PrintHelp(message, firstchar);
        break;
      case `${firstchar}marco`:
        message.reply('POLO!!!');
        break;
      case `${firstchar}profile`:
      case `${firstchar}p`:
        PrintProfile(message);
        break;
      case `${firstchar}queue`:
      case `${firstchar}q`:
        PrintSkillQueue(message, secondMessage, Client);
        break;
      case `${firstchar}register`:
      case `${firstchar}r`:
        RegisterUser(message, Client);
        break;
      case `${firstchar}system`:
      case `${firstchar}ss`:
        PrintSolarSystemInfo(message);
        break;
    }
  }

  /**
   * Welcomes a user to the server and tells them to register ASAP
   * @param {Object} member - The Discord Member
   */
  welcomeUser(member) {
    welcomeChannel = guild.channels.find('id', Configuration.WelcomeChannelId);
    serverChannel.send(`Welcome ${member.user}! Please register your EVE account with the bot in #register as soon as possible!`);
  }
}

exports.EveBot = EveBot;