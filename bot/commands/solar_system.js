const {IsValidItemArray} = require('../helpers/item_helper');
const {RetrieveQuotedItem} = require('../helpers/message_helper');

const eveEsi = require('../../eve-esi/eve-esi')

/**
 * Responds to the message of the user with the given Solar System information
 * @param {Object} message - The Discord Message
 */
async function PrintSolarSystemInfo(message) {
  let solarSystemName = RetrieveQuotedItem(message, 'solar system', 3);
  if (!solarSystemName) {
    return;
  }
  
  let messageBody = {
    embed: {
      color: 3329330,
      title: `🌟 Solar System Information for ${solarSystemName} 🌟`,
      fields: [{
        name: "Pending...",
        value: 'Please wait, retrieving data...'
      }]
    }
  };
  const fetchingMessage = await message.reply(messageBody);
  const solarSystems = await eveEsi.search(solarSystemName, 'solar_system', true);

  if (!IsValidItemArray(solarSystems.solar_system, messageBody, fetchingMessage, 'solar_system')) {
    return;
  }

  const respectiveSystem = await eveEsi.solarSystemInfo(solarSystems.solar_system[0]);

  // Get the number of asteroid belts
  let beltCount = getAsteroidBeltCount(respectiveSystem);

  // Lastly, get the constellation it's in
  const constellation = await eveEsi.constellationInfo(respectiveSystem.constellation_id);
  
  messageBody.embed = {
    fields: [{
      name: 'Name',
      value: respectiveSystem.name
    }, {
      name: 'Station Count',
      value: respectiveSystem.stations? `${respectiveSystem.stations.length}` : 0
    }, {
      name: 'Security Status',
      value: `${respectiveSystem.security_status}`
    }, {
      name: 'Asteroid Belt Count',
      value: `${beltCount}`
    }, {
      name: 'Constellation',
      value: `${constellation.name}`
    }]
  };

  fetchingMessage.edit(messageBody);
}

/**
 * Gets the number of asteroid belts in the given system  
 * @param {Object} solarSystem - The Solar System object
 * @returns {Number} - Returns the amount found
 */
function getAsteroidBeltCount(solarSystem) {
  let beltCount = 0;
  solarSystem.planets.forEach( planet => {
    if (planet.asteroid_belts) {
      beltCount += planet.asteroid_belts.length;
    }
  });
  return beltCount;
}

exports.PrintSolarSystemInfo = PrintSolarSystemInfo;