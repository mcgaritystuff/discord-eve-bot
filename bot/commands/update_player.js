const Mongoose = require('mongoose');
const Player = Mongoose.model('players');

const eveEsi = require('../../eve-esi/eve-esi')

/**
 * Updates the player in the database to match the latest ESI information
 * 
 * @param {Object} player - The Eve player objet from the database
 * @param {String} playerDiscordId - The player's Discord ID
 * @returns {Object} - Returns the updated player (from the database)
 */
exports.UpdatePlayer = async function(player, playerDiscordId) {
  const eveCharacter = await eveEsi.verifyProfile(player.playerSSOCode);
  console.log(`updating player ${eveCharacter.CharacterName}`);
  
  let allianceInfo;
  const character = await eveEsi.getCharacter(eveCharacter.CharacterID);

  if (character.info.alliance_id > 0) {
    allianceInfo = await eveEsi.allianceInfo(character.info.alliance_id);
  } else {
    allianceInfo = {};
  }

  await Player.updateOne(
    { "playerDiscordId": playerDiscordId },
    { 
      $set: {
        allianceInfo: allianceInfo,
        corporationInfo: character.corporation,
        playerCharacterId: eveCharacter.CharacterID,
        playerInfo: character.info,
        playerLastUpdated: Date.now(),
        playerPortrait: character.portrait.px128x128
      } 
    }
  );

  return await Player.findOne({ playerDiscordId: playerDiscordId });
}