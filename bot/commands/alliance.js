const Mongoose = require('mongoose');
const Alliance = Mongoose.model('alliances');

const {RetrieveQuotedItem} = require('../helpers/message_helper');
const {IsValidItemArray} = require('../helpers/item_helper');
const eveEsi = require('../../eve-esi/eve-esi');
const embedError = require('./embed_error');

const SEVEN_DAYS = 7*24*60*60*1000;

/**
 * Responds to the message of the user with the given Alliance information
 * @param {Object} message - The Discord Message
 */
async function PrintAllianceInfo(message) {
  let allianceName = RetrieveQuotedItem(message, 'alliance', 3);
  if (!allianceName) {
    return;
  }
  
  let messageBody = {
    embed: {
      color: 3329330,
      title: `🌟 Alliance Information for ${allianceName} 🌟`,
      fields: [{
        name: "Pending...",
        value: 'Please wait, retrieving data...'
      }]
    }
  };
  const fetchingMessage = await message.reply(messageBody);
  const alliances = await eveEsi.search(allianceName, 'alliance', true);

  if (!IsValidItemArray(alliances.alliance, messageBody, fetchingMessage, 'alliance')) {
    return;
  }
  const respectiveAlliance = await GetAlliance(alliances.alliance[0]); //await eveEsi.findAlliance(allianceName);
  
  if (respectiveAlliance.error) {
    messageBody.embed = embedError
  } else {
    messageBody.embed = {
      fields: [{
        name: 'Name (Ticker)',
        value: `${respectiveAlliance.allianceInfo.name} (${respectiveAlliance.allianceInfo.ticker})`
      }, {
        name: 'Date Founded',
        value: respectiveAlliance.allianceInfo.date_founded
      }, {
        name: 'Number of Corporations',
        value: respectiveAlliance.corporationsCount
      }],
      thumbnail: {
        url: respectiveAlliance.allianceIcon
      }
    };
  }
  fetchingMessage.edit(messageBody);
}

/**
 * Gets the alliance from the database. If it doesn't exist or is out of date,
 * it gets the latest from the API and saves it in the database (as a cache)
 * @param {Number} allianceId - The id of the alliance to get
 * @returns {Object} - Returns the respective alliance of the id given, if it exists
 */
async function GetAlliance(allianceId) {
  let respectiveAlliance = await Alliance.findOne({allianceId: allianceId});

  // Update with new information
  if (!respectiveAlliance ||
    new Date(respectiveAlliance.allianceLastUpdated) < new Date(Date.now() - SEVEN_DAYS)) 
  {
    const alliance = await eveEsi.getAlliance(allianceId);
   
    updatedAlliance = {
      allianceId: allianceId,
      allianceIcon: alliance.icons.px128x128,
      allianceInfo: alliance.info,
      allianceLastUpdated: Date.now(),
      corporationsCount: alliance.corporations.length
    }

    if (respectiveAlliance) {
      await Alliance.updateOne(respectiveAlliance, updatedAlliance)
    } else {
      await Alliance.create(updatedAlliance);
    }

    // Assign the respective alliance to the newly updated one
    respectiveAlliance = updatedAlliance;
  }

  return respectiveAlliance;
}


exports.GetAlliance = GetAlliance;
exports.PrintAllianceInfo = PrintAllianceInfo;