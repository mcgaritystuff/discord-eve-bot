const Mongoose = require('mongoose');
const Corporation = Mongoose.model('corporations');

const {GetAlliance} = require('./alliance');
const {CleanupDescription, IsValidItemArray} = require('../helpers/item_helper');
const {RetrieveQuotedItem} = require('../helpers/message_helper');

const eveEsi = require('../../eve-esi/eve-esi');
const embedError = require('./embed_error');

const SEVEN_DAYS = 7*24*60*60*1000;

/**
 * Responds to the message of the user with the given Corporation information
 * @param {Object} message - The Discord Message
 */
async function PrintCorporationInfo(message) {
  let corporationName = RetrieveQuotedItem(message, 'corporation', 3);
  if (!corporationName) {
    return;
  }
  
  let messageBody = {
    embed: {
      color: 3329330,
      title: `🌟 Corporation Information for ${corporationName} 🌟`,
      fields: [{
        name: "Pending...",
        value: 'Please wait, retrieving data...'
      }]
    }
  };
  const fetchingMessage = await message.reply(messageBody);
  const corporations = await eveEsi.search(corporationName, 'corporation', true);

  if (!IsValidItemArray(corporations.corporation, messageBody, fetchingMessage, 'corporation')) {
    return;
  }
  let respectiveCorporation = await GetCorporation(corporations.corporation[0]);

  let allianceName = respectiveCorporation.respectiveAlliance ?
                        respectiveCorporation.respectiveAlliance.allianceInfo.name : 
                        'N/A';
                  
  if (respectiveCorporation.error) {
    messageBody.embed = embedError
  } else {      
    messageBody.embed = {
      fields: [{
        name: 'Name (Ticker)',
        value: `${respectiveCorporation.corporationInfo.name} (${respectiveCorporation.corporationInfo.ticker})`
      }, {
        name: 'Date Founded',
        value: respectiveCorporation.corporationInfo.date_founded
      }, {
        name: 'Corresponding Alliance',
        value: allianceName
      }, {
        name: 'Tax Rate',
        value: `${respectiveCorporation.corporationInfo.tax_rate * 100}%`
      }, {
        name: 'Number of Members',
        value: respectiveCorporation.corporationInfo.member_count
      }, {
        name: 'Description',
        value: respectiveCorporation.corporationInfo.description
      }],
      thumbnail: {
        url: respectiveCorporation.corporationIcon
      }
    };
  }

  fetchingMessage.edit(messageBody);
}

/**
 * Gets the corporations from the database. If it doesn't exist or is out of date,
 * it gets the latest from the API and saves it in the database (as a cache)
 * @param {Number} corporationId - The id of the corporation to get
 * @returns {Object} - Returns the respective corporation of the id given, if it exists
 */
async function GetCorporation(corporationId) {
  let respectiveCorporation = await Corporation.findOne({corporationId: corporationId});
  let respectiveAlliance;
  
  // Update with new information
  if (!respectiveCorporation ||
    new Date(respectiveCorporation.corporationLastUpdated) < new Date(Date.now() - SEVEN_DAYS)) 
  {
    const corporation = await eveEsi.getCorporation(corporationId);
    corporation.info.description = CleanupDescription(corporation.info.description, 1024);
      
    updatedCorporation = {
      corporationId: corporationId,
      corporationIcon: corporation.icons.px128x128,
      corporationInfo: corporation.info,
      corporationLastUpdated: Date.now()
    }

    if (respectiveCorporation) {
      await Corporation.updateOne(respectiveCorporation, updatedCorporation);
    } else {
      await Corporation.create(updatedCorporation);
    }
    // Assign the respective corp to the newly updated one
    respectiveCorporation = updatedCorporation;
  }

  if (respectiveCorporation.corporationInfo.alliance_id) {
    respectiveAlliance = await GetAlliance(respectiveCorporation.corporationInfo.alliance_id);
  }

  // append the alliance information for reference
  respectiveCorporation.respectiveAlliance = respectiveAlliance;

  return respectiveCorporation;
}

exports.GetCorporation = GetCorporation;
exports.PrintCorporationInfo = PrintCorporationInfo;