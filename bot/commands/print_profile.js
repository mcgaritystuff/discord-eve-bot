const Mongoose = require('mongoose');
const Player = Mongoose.model('players');
const Race = Mongoose.model('races');

const { UpdatePlayer } = require('./update_player');
const ONE_DAY = 24*60*60*1000;

/**
 * Respond to the user with their EVE Profile (if registered)
 * @param {Object} message - The Discord message
 */
exports.PrintProfile = async function(message) {
  // First see if there's a player that's already registered
  let player = await Player.findOne({ playerDiscordId: message.author.id });

  // Check if the player's already registered or not.
  if (!player || !player.playerSSOCode) {
    message.reply("You need to register with me first! Type `$register`, please!")
  } else {
    let messageBody = {
      embed: {
        color: 3329330,
        title: `🌟 Profile for ${message.author.username} 🌟`,
        fields: [{
          name: "Pending...",
          value: 'Please wait, retrieving data...'
        }]
      }
    };

    const fetchingMessage = await message.reply(messageBody);

    // Get the character player id only if it's not in the DB already (updates every 24 hours). 
    // We store it in the DB to save on the calls made to the databases/api
    if (!player.playerLastUpdated || 
        new Date(player.playerLastUpdated) < new Date(Date.now() - ONE_DAY)) {
      try {
        player = await UpdatePlayer(player, message.author.id);
      } catch (error) {
        console.log(error);
        console.log(player);
        // Update the message body with an error
        messageBody.embed = {
          fields: [{
            name: 'ERROR',
            value: "There was an error updating your profile! Try again, and if still no go, contact an admin"
          }]
        };
        fetchingMessage.edit(messageBody);
        return;
      }
    }

    // Just to make the messageBody.embed easier to read, figure out the display text and stuff here
    const allianceName = player.allianceInfo.alliance_name? player.allianceInfo.alliance_name : 'N/A';
    const corporationName = player.corporationInfo.corporation_name? player.corporationInfo.corporation_name : 'N/A';
    const race = await Race.findOne({ raceId: player.playerInfo.race_id });
    
    messageBody.embed = {
      fields: [{
        name: 'Name',
        value: player.playerInfo.name
      }, {
        name: 'Race',
        value: race.name
      }, {
        name: 'Alliance - Corporation',
        value: `${allianceName} - ${corporationName}`
      }, {
        name: 'Birthday',
        value: player.playerInfo.birthday
      }],
      thumbnail: {
        'url': player.playerPortrait
      }
    };

    fetchingMessage.edit(messageBody);
  }
}