const Mongoose = require ('mongoose');
const Player = Mongoose.model('players');

const Configuration = require('../../config/configuration');
const Keys = require('../../config/keys');

/**
 * DMs the user a registration link (if they haven't already registered)
 * @param {Object} message - The Discord message
 * @param {Object} Client - The Discord Client
 */
exports.RegisterUser = async function registerUser(message, Client) {
  // First see if there's a player that's already registered
  const player = await Player.findOne({ playerDiscordId: message.author.id });

  // Check if the player's already registered or not.
  if (player && player.playerSSOCode !== "") {
    message.reply("You appear to have already registered!")
  } else {
    const user = await Client.fetchUser(message.author.id);
    
    try {
      await user.sendMessage({embed: {
        color: 3329330,
        title: '🌟 Registering your API 🌟',
        fields: [{
          name: 'Please go to the following URL, login, and authorize to register:',
          value: `${Configuration.RegisterUrl}/${doSimpleEncrypt(message.author.id)}`
        }]
      }});
      message.reply("I have DM'd you registration instructions!");
    } catch (err) {
      message.reply("There was a problem DM'ing you the instructions. Please make sure your privacy settings aren't blocking me!")
    };
  }
}

function doSimpleEncrypt(authorId) {
  let encryptedId = '';
  const idAsString = authorId.toString();
  for(let i = 0; i < idAsString.length; i++) {
    numOne = Number(idAsString[i]);
    numTwo = Keys.SimpleEncryptHash[i % Keys.SimpleEncryptHash.length].charCodeAt(0);
    resultingHex = (numOne + numTwo).toString(16);
    encryptedId += '00'.substr(0, 2 - resultingHex.length) + resultingHex;
  }
  return encryptedId;
}