const Axios = require('axios');
const Mongoose = require('mongoose');
const Player = Mongoose.model('players');
const Skill = Mongoose.model('skills');

const Configuration = require('../../config/configuration');
const eveEsi = require('../../eve-esi/eve-esi');
const SKILL_PRINT_COUNT = 15;

/**
 * Gets the player's Skill Queue from the game (next 15 skills)
 * @param {Object} message - The Discord message
 * @param {String} discordUserId - The id of the requested Discord user
 * @param {Object} client - The Discord Client instance
 * @param {Object} esi - The EVE ESI instance
 */
async function PrintSkillQueue(message, discordUserId, client, esi) {
  let hasReadUserRole = true;
  
  // We only care on changing this if it's for another user, not this one
  if (discordUserId) {
    discordUserId = discordUserId.replace(/[@<>]/g, '');

    if (message.author.id !== discordUserId) {
      hasReadUserRole = hasReadRole(message);
    }
  }

  if (hasReadUserRole) {
    displayMessage(message, discordUserId, client, esi);
  } else {
    message.reply('You do not have permission to view other user information.');
  }
}

/**
 * Checks if the user has the role to read other users
 * @param {Object} message - The Discord message
 * @returns {Boolean} - True if the user does, false if not
 */
function hasReadRole(message) {
  let hasRole = false;

  // Get the user from the server so we can check permissions
  const respectiveMember = message.channel.guild.members.find(member => {
    return member.id === message.author.id;
  });

  Configuration.RolesAllowedToReadUsers.forEach(id => {
    if (respectiveMember.roles.find(roleId => {return roleId.id === id})) {
      hasRole = true;
    }
  });

  return hasRole;
}

/**
 * Displays the embed message into the channel
 * @param {Object} message - The Discord Message
 * @param {String} discordUserId - The User's Discord Id
 * @param {Object} client - The Discord Client instance
 * @param {Object} esi - The EVE ESI instance
 */
async function displayMessage(message, discordUserId, client) {
  const user = await client.fetchUser(discordUserId? discordUserId : message.author.id);

  if (!user) {
    message.reply('ERROR: Problem fetching that user');
    return;
  }

  let messageBody = {
    embed: {
      color: 3329330,
      title: `🌟 Next 15 skills in queue for ${user.username} 🌟`,
      fields: [{
        name: "Pending...",
        value: 'Please wait, retrieving data...'
      }]
    }
  };
  const fetchingMessage = await message.reply(messageBody);

  let player = await Player.findOne({ playerDiscordId: user.id });

  // Check if the player's already registered or not.
  if (!player || !player.playerSSOCode) {
    messageBody.embed.fields = [{
      name: "ERROR",
      value: 'User is not registered'
    }];
    fetchingMessage.edit(messageBody);

    message.reply("Account needs to be registered with me first!")
    return;
  }
  // Get the skills list
  const authToken = await eveEsi.playerAuthToken(player.playerSSOCode);
  const skillsList = await eveEsi.skillQueue(player.playerCharacterId, authToken);
  
  let skillListPrintout = '';
  let countSoFar = 0;
  
  for (let i = 0; countSoFar < SKILL_PRINT_COUNT && i < skillsList.length; i++) {
    if (skillsList[i].finish_date && new Date(skillsList[i].finish_date) > Date.now()) { 
      const timeLeft = (countSoFar === 0? getTimeRemaining(new Date(), skillsList[i].finish_date) :
                                          getTimeRemaining(skillsList[i].start_date, skillsList[i].finish_date));                                        
      const skill = await Skill.findOne({skillId: skillsList[i].skill_id});
      skillListPrintout += `${countSoFar+1}. `+
                           `${skill.skillName} `+
                           `(${skillsList[i].finished_level}) ---------------- `+
                           `${timeLeft}\n`;
      countSoFar++;
    }
  }

  // User has no skills in queue
  if (skillListPrintout === '') {
    skillListPrintout = "No Skills in queue.";
  }
  
  messageBody.embed.fields = [{
    name: "Skill (Level) ---------------- Time To Finish",
    value: skillListPrintout
  }];
  fetchingMessage.edit(messageBody);
}

function getTimeRemaining(startTime, endTime){
  var t = Date.parse(endTime) - Date.parse(startTime);
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) % 7 );
  var weeks = Math.floor( t/(1000*60*60*24*7) );
  
  if (weeks > 0) {
    return `${weeks}w ${days}d ${hours}h ${minutes}m`
  } else if (days > 0) {
    return `${days}d ${hours}h ${minutes}m`;
  } else if (hours > 0) {
    return `${hours}h ${minutes}m`;
  } else {
    return `${minutes}m`;
  }
  
}

exports.PrintSkillQueue = PrintSkillQueue;