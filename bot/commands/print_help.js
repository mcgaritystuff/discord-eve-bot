/**
 * Replies to the user with a help message
 * @param {Object} message - The Discord message
 */
exports.PrintHelp = function(message, discordSymbol) {
  message.reply({embed:{
    color: 3329330,
    title: "🌟 EVE-Bot Help 🌟",
    fields: [{
        name: "WHAT IS THIS!?",
        value: "EVE Bot is what it sounds like: A bot for EVE. "+
               "EVE Bot provides various functionalities. Please see below for all the infoz"
      }, {
        name: "Alliance Information (Command):",
        value: `\`${discordSymbol}alliance\`/\`${discordSymbol}a\` "<alliance name>" => Shows information about the given alliance. Updated at least weekly.`
      }, {
        name: "Corporation Information (Command):",
        value: `\`${discordSymbol}corporation\`/\`${discordSymbol}c\` "<corporation name>" => Shows information about the given corporation. Updated  at least weekly.`
      }, {
        name: "Marco Test (Command):",
        value: `\`${discordSymbol}marco\` => Replies with Polo`
      }, {
        name: "Profile Information (Command):",
        value: `\`${discordSymbol}profile\`/\`${discordSymbol}p\` => Show your profile from Eve. Updated at least every 24 hours.`
      }, {
        name: "Skill Queue (Command):",
        value: `\`${discordSymbol}queue\`/\`${discordSymbol}q\` => Show your skill queue for Eve.`
      }, {
        name: "Register Character (Command):",
        value: `\`${discordSymbol}register\`/\`${discordSymbol}r\` => Register your API to the bot`
      }, {
        name: "Solar System Information (Command):",
        value: `\`${discordSymbol}system\`/\`${discordSymbol}ss\` => Show information about the given solar system.`
      }
    ]
  }});
}