module.exports = {
  fields: [{
    name: 'Error',
    value: 'There was an error processing the request.'
  }]
}