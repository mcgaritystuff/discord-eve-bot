/**
 * Validates that the array provide has exactly one item. If not, the
 * message is updated with the proper details.
 * @param {Array} idArray - The array of id numbers
 * @param {Object} messageBody - The embed message body for the fetchingMessage
 * @param {Object} fetchingMessage - The message that was sent with 'pending'
 * @param {String} typeOfItem - The type of item (i.e. 'alliance', 'corporation', etc.)
 * @returns {Boolean} - True = valid message, false = invalid message
 */
exports.IsValidItemArray = function(idArray, messageBody, fetchingMessage, typeOfItem) {
  // No items found
  if (!idArray || idArray.length === 0) {
    messageBody.embed.fields = [{
      name: 'ERROR',
      value: `Invalid ${typeOfItem}! Please check your spelling`
    }];
    fetchingMessage.edit(messageBody);
    return false;
  }

  // Make sure we only have one result returned
  if (idArray.length > 1) {
    messageBody.embed.fields = [{
      name: 'ERROR',
      value: `Found more than one ${typeOfItem}! Please put the exact name (or if applicable, ticker)`
    }];
    fetchingMessage.edit(messageBody);
    return false;
  }

  return true;
}

/**
 * Cleans up a description by removing the annoying html elements and 
 * restricts it to the maxCharLength
 * @param {String} description - The description given by the item
 * @param {Number} maxCharLength - The max number of chars to return in it
 * @returns {String} - The description, cleaned up
 */
exports.CleanupDescription = function(description, maxCharLength) {
  let newDescription = description.replace(/<br>/g, '\n');
  newDescription = newDescription.replace(/<[^>]*>/g, '');
  newDescription = newDescription.substring(0, maxCharLength);

  if (newDescription.length === maxCharLength) {
    newDescription = newDescription.substring(0, maxCharLength - 4) + '....';
  }

  return newDescription;
}