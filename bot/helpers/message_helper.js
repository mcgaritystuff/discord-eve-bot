/**
 * Gets the item name parsed out of the message
 * @param {Object} message - The message to reply to (if errored) and read contents from
 * @param {String} typeOfItem - The type of item (i.e. 'alliance', 'corporation', etc)
 * @param {Number} minCharCount - The minimum number of chars allowed for the string
 * @returns {String} - The itemName if valid input, null if there was invalid input
 */
exports.RetrieveQuotedItem = function(message, typeOfItem, minCharCount) {
  let itemName;
  const contents = message.content.split(" ");
  const matches = message.content.match(/["']([^"']*?)["']/);

  // No name given or empty quoted string
  if (contents.length < 2 || contents[1] === '""' || contents[1] === "''") {
    message.reply(`Please give the ${typeOfItem} name!`);
    return null;
  } else if (contents.length > 2) {
    // If there's more than one set of contents without quotes to make them one, then inform the user 
    if (!matches || matches.length < 2) {
      message.reply(`You must include the ${typeOfItem} name in quotes if it's more than `+
                    `one word (i.e. \`$command "awesome ${typeOfItem}")\`.`);
      return null;
    } else {
      itemName = matches[1];
    }
  } else {
    if (!matches || matches.length < 2) {
      itemName = contents[1];
    } else {
      itemName = matches[1];
    }
  }

  if (itemName.length < minCharCount) {
    message.reply(`The name of the ${typeOfItem} requires at least ${minCharCount} characters`);
    return null;
  }

  return itemName;
}