const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const playerSchema = new Schema({
  allianceInfo: Object,
  corporationInfo: Object,
  playerCharacterId: Number,
  playerInfo: Object,
  playerDiscordId: String,
  playerLastUpdated: Date,
  playerPortrait: Object,
  playerSSOCode: String,
  playerSSOState: String
});

Mongoose.model('players', playerSchema);
