const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const corporationSchema = new Schema({
  corporationId: Number,
  corporationIcon: String,
  corporationInfo: Object,
  corporationLastUpdated: Date
});

Mongoose.model('corporations', corporationSchema);
