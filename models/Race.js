const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const raceSchema = new Schema({
  allianceId: Number,
  description: String,
  name: String,
  raceId: Number
});

Mongoose.model('races', raceSchema);
