const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const allianceSchema = new Schema({
  allianceId: Number,
  allianceIcon: String,
  allianceInfo: Object,
  allianceLastUpdated: Date,
  corporationsCount: Number
});

Mongoose.model('alliances', allianceSchema);
