const Mongoose = require('mongoose');
const { Schema } = Mongoose;

const skillSchema = new Schema({
  skillId: Number,
  skillName: String,
  skillDescription: String
});

Mongoose.model('skills', skillSchema);
